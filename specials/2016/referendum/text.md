---
title: Waarom is er zo veel te doen uitgerekend over dít verdrag?
---
test123
## Ophef
Het eurosceptische Geenstijl was op zoek naar Europese maatregelen die de samenwerking tussen landen vergroten. Het associatieverdrag tussen de Europese Unie en Oekraïne bleek het meest actueel en werd door het weblog geadopteerd. Het toeval dat juist dit verdrag voor handen was, is dus een belangrijke reden voor de ophef.

---
title: Wat is een ’raadgevend’ referendum eigenlijk?
---
## Raadgevend
Het referendum over het associatieverdrag is het eerste ‘raadgevende’ referendum in Nederland vanuit een burgerinitiatief. Het referendum is dus niet bindend voor het parlement. Pas sinds 1 juli 2015 bestaat de mogelijkheid om - eerst met 10.000 en later met 300.000 handtekeningen - een referendum af te dwingen. In 2005 mocht Nederland voor het eerst naar de stembus voor een referendum. Toen stond de Europese grondwet op het spel. In Nederland was ook dat referendum een soort advies voor de Tweede Kamer. Het verschil met nu zit dus in de aanjagers: in 2005 kwam het referendum voort uit een initiatiefwet van D66, GroenLinks en de PvdA, nu vanuit weblog Geenstijl.

---
title: Wat staat er nu precies in?
---
## Wat staat erin?

→  Hier een puntsgewijze samenvatting van het verdrag
Volgt van Bert.


---
title: Is dit een bijzonder verdrag?
---
## Bijzonderheid
Is dit een bijzonder verdrag?
Voorbeelden van soortgelijke verdragen: Turkije (1963), Jordanië (2002), Israël, Marokko en Chili.
→ Hier toont een eenvoudige, niet al te drukke animatie hoe veel vergelijkbare verdragen wel niet worden en zijn gesloten.

---
title: Hoe kan het dat het verdrag al voor een deel in werking is getreden?
---
## Deel al in werking
Gaat het verdrag pas in als Nederland er zijn goedkeuring aan heeft gegeven?
Nee. Sommige punten in het verdrag treden al in werking voordat elke lidstaat afzonderlijk zijn goedkeuring heeft gegeven. Dat geldt dus ook voor het referendum in Nederland.

Het verdrag omvat met name economische afspraken. De makers van het verdrag achtten het belangrijker om voor die afspraken al uit te voeren, in plaats van te wachten op de goedkeuring van elke lidstaat.

---
title: Wat vindt politiek Den Haag ervan?
---
## Den Haag
Hier een graphic met kabinet en politieke partijen en bij ieder kort: voor of tegen? wel of niet actief campagne? Moet nog worden aangevuld door Fokke.

---
title: Wat zijn de scenario's?
---
## Scenario's
Wat zijn de scenario’s?
De scenario’s. Slider:
1.  Onvoldoende opkomst en meerderheid voor. Ligt niet voor de hand.
2.  Onvoldoende opkomst en meerderheid tegen
3.  Voldoende opkomst en meerderheid voor
4.  Voldoende opkomst en nipte meerderheid tegen. Lastig maar regering kan dit negeren),
5.  Voldoende opkomst en ruime meerderheid tegen. Regering kan uitslag moeilijk naast zich neerleggen.

---
title: Wat vindt men in Oekraïne
---
In het gewapende conflict in Oost-Oekraïne zijn de verhoudingen duidelijk. De pro-Europese groeperingen hopen dat het verdrag wordt aangenomen, de pro-Russische rebellen wrijven in hun handen als het verdrag wordt verworpen.

---
title: Stem voor = Meer EU en omgekeerd
---
Is dit referendum geschikt om je onvrede over de EU te uiten?
Ook hierover staat niets concreets in het verdrag. Toch geven veel voor- en tegenstanders van het verdrag wel een dergelijke betekenis aan het referendum. Indirect zeggen zij: een stem voor het verdrag is een stem voor verdere Europese samenwerking, een stem tegen het verdrag is een stem tegen verdere Europese samenwerking.

In het verdrag staat nergens dat dit de eerste stap is tot toetreding van de Europese Unie. Tegenstanders menen toch dat de overeenkomst een voorbode is van verdere samenwerking. Ze dragen Kroatië aan als voorbeeld; een land waarmee óók een associatieverdrag werd gesloten en dat nu één van de 28 lidstaten is. Voorstanders wijzen op Chili, Jordanië, Turkije en Israël. Die landen ondertekenden eveneens een soortgelijk verdrag, maar het is onwaarschijnlijk dat zij op termijn toetreden tot de EU.

---
title: Kun je met een tegenstem Poetin in de kaart spelen?
---
## Poetin
Met het oog op de Russische invloedsfeer in Rusland ziet hij waarschijnlijk het verdrag liever verworpen dan aangenomen worden. Het is alleen te zwart-wit om te stellen dat een stem tegen het verdrag een stem voor Poetin is, mede omdat Poetin zich in het openbaar niet uitspreekt over het referendum in Nederland.